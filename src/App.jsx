import React from "react";
import IllustrationWomanOnlineMobile from "../src/assets/svg/illustration-woman-online-mobile.svg";
import PatternMobile from "../src/assets/svg/bg-pattern-mobile.svg";
import IllustrationWomanOnlineDesktop from "../src/assets/svg/illustration-woman-online-desktop.svg";
import PatternDesktop from "../src/assets/svg/bg-pattern-desktop.svg";
import BoxDesktop from "../src/assets/svg/illustration-box-desktop.svg";

import "./styles/index.css";
import "./App.css";
import CardFAQ from "./components/CardFAQ";

const faqs = [
  {
    id: 1,
    title: "How many team members can I invite?",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, aenean porta dui dolor.",
    state: "inactive",
  },
  {
    id: 2,
    title: "What is the maximum file upload size?",
    description:
      "No more than 2GB. All files in your account must fit your allotted storage space.",
    state: "inactive",
  },
  {
    id: 3,
    title: "How do I reset my password?",
    description:
      "At egestas ante rutrum nec. Phasellus malesuada tempor lectus.",
    state: "inactive",
  },
  {
    id: 4,
    title: "Can I cancel my subscription?",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, aenean porta dui dolor.",
    state: "inactive",
  },
  {
    id: 5,
    title: "Do you provide additional support?",
    description:
      "At egestas ante rutrum nec. Phasellus malesuada tempor lectus.",
    state: "inactive",
  },
];

function App() {
  return (
    <main className="App">
      <section className="section-faq l-container">
        <div className="content-faq">
          <div className="decorative--mobile">
            <img
              className="decorative-top"
              src={IllustrationWomanOnlineMobile}
              alt="Illustration woman online mobile"
            />
            <img
              className="decorative-bottom"
              src={PatternMobile}
              alt="Pattern Mobile"
            />
          </div>
          <div className="decorative--desktop">
            <img
              className="decorative-top"
              src={PatternDesktop}
              alt="Illustration woman online mobile"
            />
            <img
              className="decorative-center"
              src={IllustrationWomanOnlineDesktop}
              alt="Illustration woman online mobile"
            />
          </div>
          <div className="questions">
            <h1 className="title-faq">faq</h1>
            <CardFAQ data={faqs} />
          </div>
        </div>
        <img
          className="decorative-bottom"
          src={BoxDesktop}
          alt="Illustration woman online mobile"
        />
      </section>
    </main>
  );
}

export default App;
